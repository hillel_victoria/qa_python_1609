# a = 1
# b = 2
#
# c = a + b
#
# print(c)

students_counter = 30 # snake case
studentsCounter = 30 # camel case

studentsCounter = 'qwerty'

# typing

# a = 10
# b = '20'
# c = a + b
#
# print (c)


#data types

#int


my_int1 = 10
my_int2 = 10

res = my_int1 + my_int2
res = my_int1 - my_int2
res = my_int1 * my_int2
res = my_int1 / my_int2
res = my_int1 ** my_int2 # степінь
res = my_int1 // my_int2 # ділення до цілого числа
res = my_int1 % my_int2 # залишок від ділення на 3 10%3 -> 10-3*3
res = 4 % 2
print(res)

my_int1 = my_int1 + my_int2
print(my_int1)
my_int1 += my_int2 # this is the same, but better

my_int1 *= my_int2
print(my_int1)


# float

my_float1 = 1.2
my_float2 = 4.5

res = my_float1 + my_float2
print(res)

print (5.7 == my_float1 + my_float2)


res = my_float1 - my_float2
print(res)

res = my_float1 * my_float2
print(res)

res = my_float1 / my_float2
print(res)

res = my_float1 ** my_float2
print(res)

res = my_int1 - my_float2
print(res)

my_float1 = 0.1
my_float2 = 0.2

res = my_float1 + my_float2
print (res)

print (0.3 == my_float1 + my_float2)

my_float1 = 0.00000000000000000000003
print(my_float1)

my_float1_exp = 3e-20 # 3 * (10 ** -20)
print(my_float1_exp)

# print(my_float1 / 0) # error

# boolean

my_bool1 = True
my_bools = False

my_bool = my_float1 > my_int1
print(my_bool)

# print(type(my_bool))
# print(type(my_int1))

my_bool = my_float1 >= my_int1
my_bool = my_float1 == my_int1
my_bool - my_float1 != my_int1

print(my_bool)

# test 12345
